# Simple 'To Do' application
This is simple task management application made for fun and educational purposes. 
It is based on [Angular2](http://angular.io) [quickstart project](https://angular.io/docs/ts/latest/quickstart.html). 

## Prerequsites

- https://www.mongodb.com/
- http://restheart.org/

## Installation and run

Get the application:

    git clone https://Pawlik7@bitbucket.org/Pawlik7/todo-app.git
    cd todo-app
    npm install

Run mongoDB (default configuration is fine):

    mongod

and populate some data:

    mongo localhost:27017/todo-app populate.js

[run RESTHeart](https://softinstigate.atlassian.net/wiki/display/RH/Installation+and+Setup):

    java -jar restheart.jar 

and finally run application

    npm start

enjoy :-)

## Some thoughts on technology stack

Enabling technologies:

- HTML5
- JavaScript --> TypeScript
- CSS

Enabling tools (some of them):

- MEAN
- Visual Studio Code 
- Google Chrome
- git

And interesting [article](http://blog.octo.com/en/new-web-application-architectures-and-impacts-for-enterprises-1/) on technology evolution.


Few excerpts from wiki:

https://en.wikipedia.org/wiki/Node.js
Node.js is an open-source, cross-platform JavaScript runtime environment for developing a diverse variety of tools and applications.

https://en.wikipedia.org/wiki/Npm_(software)
npm is the default package manager for the JavaScript runtime environment Node.js.

https://en.wikipedia.org/wiki/AngularJS
AngularJS (commonly referred to as "Angular" or "Angular.js") is a complete JavaScript-based open-source front-end web application framework mainly maintained by Google and by a community of individuals and corporations to address many of the challenges encountered in developing single-page applications.

https://en.wikipedia.org/wiki/Single-page_application
A single-page application (SPA) 
An SPA moves logic from the server to the client. This results in the role of the web server evolving into a pure data API or web service. This architectural shift has, in some circles, been coined "Thin Server Architecture" to highlight that complexity has been moved from the server to the client, with the argument that this ultimately reduces overall complexity of the system.

https://en.wikipedia.org/wiki/MEAN_(software_bundle)
MEAN is a free and open-source JavaScript software stack for building dynamic web sites and web applications.[1]
The MEAN stack makes use of MongoDB, Express.js, Angular, and Node.js. 

https://en.wikipedia.org/wiki/TypeScript
TypeScript is a free and open source programming language developed and maintained by Microsoft. It is a strict superset of JavaScript,