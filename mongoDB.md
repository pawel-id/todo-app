# mongoDB first steps

# what is it?
MongoDB is NoSQL document database.

Hierarchy: 

- database (eg: use todo-app, db),
- collection (eg: db.todos.insert(...)}
- document

compared to [mySQL](https://www.mongodb.com/compare/mongodb-mysql)

## download and install
[Download](https://www.mongodb.com/download-center#community) and install package for your operating system. In my case (windows 7 and 10) package "Windows 2008 R2...64bit, with SSL..." worked fine.

## prepare for the first run
Follow the official [instruction](https://docs.mongodb.com/manual/installation/) or follow my super simple steps:

add mongo bin directory to your path (for convenience).

create directories (mongoDB defaults):

    mkdir c:\data
	mkdir c:\data\db
	mkdir c:\data\log

## run 
run database:

    mongod

## play

    mongo

## basic commands

<https://docs.mongodb.com/manual/reference/method/js-collection/>

use datadase
db
db.todos.insert({done: false, task: "do it"})
db.todos.find({tags: {$exists: false}})
db.todos.updateMany({tags: {$exists: false}}, {$set: {tags: ["anything"]}})

## other nice tools

<https://robomongo.org/>

<http://restheart.org/>

## further steps

nice youtube intro <https://www.youtube.com/watch?v=pWbMrx5rVBE>

working app in 30min <https://www.sitepoint.com/deploy-rest-api-in-30-mins-mlab-heroku/>



