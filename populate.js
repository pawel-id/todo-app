/*
Script for database population with some sample data
    mongo localhost:27017/todo-app populate.js
*/

// conn = new Mongo();
// db = conn.getDB('dict');

// Warning: dropping database here
print('dropping database');
db.dropDatabase();

print('inserting todos');

db.todos.insertOne({
    done: false, 
    task: "zrób coś"});

db.todos.insertOne({
    done: false, 
    task: "zrób jeszcze coś"});

db.todos.insertOne({
    done: false, 
    task: "zrób byle co"});

db.todos.insertOne({
    done: false, 
    task: "leć na marsa",
    description: "to będzie piękna podróż!",
    tags: ["trip","space"]
});


print('todos inserted');

