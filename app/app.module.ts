import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule }    from "@angular/http";

import { AppComponent } from "./app.component";
import { TodoListComponent } from "./todo-list.component";
import { TodoService } from "./todo.service";

@NgModule({
  imports: [BrowserModule, HttpModule ],
  declarations: [AppComponent, TodoListComponent],
  providers: [ TodoService ],
  bootstrap: [AppComponent],
})

export class AppModule { }
