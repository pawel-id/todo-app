import { Component, OnInit } from "@angular/core";
import { Todo } from "./todo";
import { TodoService } from "./todo.service";


/*
const TODOS: Todo[] = [
    {done: false, task: "zrobić zakupy"},
    {done: true, task: "wyprowadzić psa"},
];
*/

@Component({
    selector: "todo-list",
    templateUrl: "app/todo-list.component.html",
    styles: [ ".done { text-decoration: line-through; }" ],
})

export class TodoListComponent implements OnInit {

    todos: Todo[] = [];
    // todos = TODOS;

    constructor(private todoService: TodoService) { }

    addTodo(todoStr: string) {
        let todo: Todo = {_id: undefined, done: false, task: todoStr};
        this.todos.push(todo);
        this.todoService.addTodo(todo).then(() => todo);
    }

    toggleTodo(todo) {
        todo.done = todo.done ? false : true;
        this.todoService.updateTodo(todo).then(() => todo);
    }

    ngOnInit() {
        this.todoService.getTodos().then(todos => this.todos = todos);
     }
}
