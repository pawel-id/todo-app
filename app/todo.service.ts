import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import "rxjs/add/operator/toPromise";

import { Todo } from "./todo";

@Injectable()
export class TodoService {

    private headers = new Headers({ "Content-Type": "application/json" });
    private url = "http://127.0.0.1:8080/todo-app/todos/";

    constructor(private http: Http) { }

    getTodos(): Promise<Todo[]> {
        //    return Promise.resolve(TODOS);

        return this.http.get(this.url, { headers: this.headers })
            .toPromise()
            .then(response => response.json()["_embedded"]["rh:doc"] as Todo[])
            .catch(this.handleError);
    }

    updateTodo(todo: Todo): Promise<Todo> {

        let _url = this.url + todo._id["$oid"];
        let _todoJson = JSON.stringify(todo);

        return this.http.put(_url, _todoJson, { headers: this.headers })
            .toPromise()
            .then(() => todo)
            .catch(this.handleError);
    }

    addTodo(todo: Todo): Promise<Todo> {
        let _url = this.url;
        let _todoJson = JSON.stringify(todo);

        if (todo._id === null || todo._id === undefined) {
            _todoJson = JSON.stringify(todo, (key, value) => (key === undefined) ? undefined : value)
        };

        return this.http.post(_url, _todoJson, { headers: this.headers })
            .toPromise()
            .then((response) => {
                let _l = response.headers.get("Location");
                let _lt = _l.split("/");
                let _id = _lt[_lt.length - 1];
                todo._id = _id;
                return todo;
            })
            .catch(this.handleError);

    }

    private handleError(error: any): Promise<any> {
        console.error("An error occurred", error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
