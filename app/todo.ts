export class Todo {
    _id: string;
    done: boolean;
    task: string;
}
